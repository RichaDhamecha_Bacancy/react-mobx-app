import React, { Component } from 'react';
import TodoPage from "./page/TodoPage";
import Todo from "./components/Todo";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class AppRouter extends Component {
  constructor(props) {
    super(props);
    this.todoPage = new TodoPage();
  }

  render() {
    return (
      <Router>
        <div>
          <nav>
            <ul style={{listStyleType : 'none'}}>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/todos/">Todos</Link>
              </li>
            </ul>
          </nav>

          <Route path="/" exact render={() => <h2>Home</h2>} />
          <Route path="/todos/" render={() => <Todo store={this.todoPage} todos={this.todoPage.todos} />} />
        </div>
      </Router>
    );
  }
}

export default AppRouter;
