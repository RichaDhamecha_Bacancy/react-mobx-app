import { observable } from "mobx";

export default class TodoPage {
  @observable todos = [];
  @observable todo = "";

  constructor() {
    this.todos = [
      { id: 1, body: "ABC", completed: false },
      { id: 2, body: "DEF", completed: false },
      { id: 3, body: "GHI", completed: false },
      { id: 4, body: "JKL", completed: false },
      { id: 5, body: "MNO", completed: false }
    ];
  }

  onClickTodo(id) {
    let currentTodo = this.todos.find(
      todo => todo.id === id && !todo.completed
    );
    this.todo = currentTodo ? currentTodo.body : '';
  }

  onClickAddTodo = () => {
    if (this.todo) {
      let lastTodo = this.todos[this.todos.length - 1];
      lastTodo && this.todos.push({ id: Math.random() * 200, body: this.todo, completed: false });
      this.todo = "";
    } else {
      alert("Enter some value in todo");
    }
  };

  onClickDeleteTodo(id) {
    let index = this.todos.findIndex(todo => todo.id === id);
    if (index > -1) {
      this.todos.splice(index, 1);
    }
    this.todo = "";
  }

  onClickDoneTodo(id) {
    let index = this.todos.findIndex(todo => todo.id === id);
    if (index > -1) {
      this.todos[index].completed = true;
    }
    this.todo = "";
  }
}
