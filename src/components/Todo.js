import React, { Component } from "react";
import { observer } from "mobx-react";

@observer
export default class Todo extends Component {
  render() {
    const style = {
      color: "green",
      textAlign: "left"
    };
    return (
      <div>
        <div>
          <ul>
            {this.props.todos &&
              this.props.todos.map((todo, key) => {
                return (
                  <React.Fragment key={todo.id}>
                    <li
                      style={style}
                      onClick={e => this.props.store.onClickTodo(todo.id)}
                    >
                      {todo.completed ? (
                        <del>
                          <h3>{todo.body}</h3>
                        </del>
                      ) : (
                        <h3>{todo.body}</h3>
                      )}
                    </li>
                    <button
                      style={{ display: "flex", fontSize: "15px" }}
                      onClick={e => this.props.store.onClickDoneTodo(todo.id)}
                    >
                      Done
                    </button>
                    <button
                      style={{ display: "flex", fontSize: "15px" }}
                      onClick={e => this.props.store.onClickDeleteTodo(todo.id)}
                    >
                      Delete
                    </button>
                  </React.Fragment>
                );
              })}
          </ul>
        </div>
        <div
          style={{ display: "flex", paddingLeft: "5px", paddingRight: "5px" }}
        >
          <input
            type="text"
            name="todo"
            value={this.props.store.todo}
            onChange={e => (this.props.store.todo = e.target.value)}
          />
          <button
            style={{
              marginLeft: "10px",
              backgroundColor: "#61dafb"
            }}
            onClick={this.props.store.onClickAddTodo}
          >
            Add Todo
          </button>
        </div>
      </div>
    );
  }
}
